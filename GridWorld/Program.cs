﻿using GridWorld.Controlers;
using GridWorld.Interfaces;
using GridWorld.Models;
using GridWorld.Strategies;
using GridWorld.Utilities;
using GridWorld.Views;

namespace GridWorld
{
    internal class Program
    {
        static void Main(string[] args)
        {
            IGridView gridView = new ConsoleGridView();

            int width = InputUtility.GetInputValue(gridView, "Enter the grid width: ");
            int height = InputUtility.GetInputValue(gridView, "Enter the grid height: ");
            int maxIteration = InputUtility.GetInputValue(gridView, "Enter maximum number of iteration: ");

        
            IGrid grid = new Grid(width, height);
            IMovementStrategy movementStrategy = new RandomMovementStrategy(grid);
            IPlayer player = new DummyPlayer(grid, movementStrategy, maxIteration);


            var controller = new MainController(grid, player, gridView);

            controller.Start();

        }

    }
}