﻿using GridWorld.Models;

namespace GridWorld.Interfaces
{
    public interface IPlayer
    {
        void Move(Vector2Int position);
        Vector2Int? ChooseNextStep();
        bool HasReachedGoal();
        bool CanMakeMoreMoves(int moves);
        bool IsCompletelyBlocked();
        void Backtrack();
        IEnumerable<Vector2Int> GetPath();
    }
}
