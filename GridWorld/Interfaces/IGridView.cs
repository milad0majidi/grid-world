﻿using GridWorld.Enums;

namespace GridWorld.Interfaces
{
    public interface IGridView
    {
        void DisplayGrid(CellType[,] grid, int x, int y);

        void DisplayMessage(string message);
    }
}
