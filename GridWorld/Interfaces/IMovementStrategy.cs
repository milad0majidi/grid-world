﻿using GridWorld.Models;

namespace GridWorld.Interfaces
{
    public interface IMovementStrategy
    {
        Vector2Int? ChooseNextStep(Vector2Int currentCell, HashSet<Vector2Int> visitedCells);
        Vector2Int? GetPreviousStep(Stack<Vector2Int> pathStack);
    }
}
