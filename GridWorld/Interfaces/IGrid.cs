﻿using GridWorld.Enums;
using GridWorld.Models;

namespace GridWorld.Interfaces
{
    public interface IGrid
    {
        CellType[,] Tiles { get; }
        bool IsValidCell(int x, int y);
        Vector2Int GetGoalCell();
        Vector2Int GetStartCell();
    }

}
