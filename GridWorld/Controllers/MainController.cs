﻿using GridWorld.Interfaces;

namespace GridWorld.Controlers
{
    internal class MainController
    {
        private readonly IGrid grid;
        private readonly IPlayer player;
        private readonly IGridView view;
        private const int waitTime = 500;

        public MainController(IGrid grid, IPlayer player, IGridView view)
        {
            this.grid = grid;
            this.player = player;
            this.view = view;
        }

        public void Start()
        {
            int iterationsMade = 0;

            do
            {
                var nextStep = player.ChooseNextStep();

                // Check if player is blocked and no next step is available.
                if (!nextStep.HasValue && player.IsCompletelyBlocked())
                {
                    view.DisplayMessage("No more moves possible. Stopping...");
                    break; // Exit the loop as we're completely blocked 
                }

                // If there's no next step, but the player isn't completely blocked, backtrack.
                if (!nextStep.HasValue)
                {
                    player.Backtrack();
                    view.DisplayMessage("Dead-end reached. Backtracking...");
                    continue;
                }

                // If there's a next step, move the player and update the view.
                player.Move(nextStep.Value);
                view.DisplayGrid(grid.Tiles, nextStep.Value.X, nextStep.Value.Y);
                iterationsMade++;

                Thread.Sleep(waitTime);

            } while (!player.HasReachedGoal() && player.CanMakeMoreMoves(iterationsMade));

            DisplayGameOutcome();

            view.DisplayMessage($"Iterations Made: {iterationsMade}");
        }


        private void DisplayGameOutcome()
        {
            if (player.HasReachedGoal())
            {
                view.DisplayMessage("Player reached the goal!");
            }
            else
            {
                view.DisplayMessage("Moves exhausted before reaching the goal.");
            }
        }
    }
}
