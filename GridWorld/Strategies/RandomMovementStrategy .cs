﻿using GridWorld.Interfaces;
using GridWorld.Models;

namespace GridWorld.Strategies
{
    public class RandomMovementStrategy : IMovementStrategy
    {
        private static readonly Random random = new Random();

        private readonly IGrid grid;
        public RandomMovementStrategy(IGrid grid)
        {
            this.grid = grid;
        }

        public Vector2Int? ChooseNextStep(Vector2Int currentCell, HashSet<Vector2Int> visitedCells)
        {
            var possibleCells = GetPossibleCells(currentCell);
            List<Vector2Int> unvisitedCells = new List<Vector2Int>();

            foreach (var cell in possibleCells)
            {
                if (grid.IsValidCell(cell.X, cell.Y) && !visitedCells.Contains(cell))
                {
                    unvisitedCells.Add(cell);
                }
            }

            if (unvisitedCells.Count == 0)
            {
                return null; // No valid moves available
            }

            return GetMove(unvisitedCells);
        }
        public Vector2Int? GetPreviousStep(Stack<Vector2Int> pathStack)
        {
            if (pathStack.Count > 0)
            {
                pathStack.Pop(); // Pop the current cell
                if (pathStack.TryPeek(out var currentCell))
                    return currentCell;
            }
            return null;
        }

        private static Vector2Int GetMove(List<Vector2Int> possibleMoves)
        {
            return possibleMoves[random.Next(possibleMoves.Count)];
        }
       
        private static List<Vector2Int> GetPossibleCells(Vector2Int currentPosition)
        {
            return new List<Vector2Int>
            {
                currentPosition + Vector2Int.Up,
                currentPosition + Vector2Int.Down,
                currentPosition + Vector2Int.Left,
                currentPosition + Vector2Int.Right,
            };
        }
    }
}
