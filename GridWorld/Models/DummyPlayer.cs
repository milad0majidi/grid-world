﻿using GridWorld.Interfaces;

namespace GridWorld.Models
{
    public class DummyPlayer : IPlayer
    {
        private readonly IGrid grid;
        private readonly IMovementStrategy movementStrategy;

        private readonly HashSet<Vector2Int> visitedCells = new HashSet<Vector2Int>();
        private readonly Stack<Vector2Int> pathStack = new Stack<Vector2Int>();

        private Vector2Int currentCell;
        private readonly int maxMoves;

        public bool HasReachedGoal() => currentCell == grid.GetGoalCell();
        public bool IsCompletelyBlocked() => pathStack.Count <= 1;
        public IEnumerable<Vector2Int> GetPath() => pathStack.Reverse();

        public DummyPlayer(IGrid grid, IMovementStrategy movementStrategy, int maxIteration)
        {
            this.grid = grid;
            this.movementStrategy = movementStrategy;
            this.maxMoves = maxIteration;

            ResetPosition();
        }

        private void ResetPosition()
        {
            currentCell = grid.GetStartCell();
            visitedCells.Clear();
            visitedCells.Add(currentCell);
            pathStack.Push(currentCell);
        }

        public Vector2Int? ChooseNextStep()
        {
            return movementStrategy.ChooseNextStep(currentCell, visitedCells);
        }

        public void Move(Vector2Int cell)
        {
            if (!grid.IsValidCell(cell.X, cell.Y))
            {
                return;
            }

            currentCell = cell;
            visitedCells.Add(currentCell);
            pathStack.Push(currentCell);
        }

        public void Backtrack()
        {
            var previousCell = movementStrategy.GetPreviousStep(pathStack);

            if (previousCell != null)
            {
                currentCell = (Vector2Int)previousCell;
                return;
            }

            // If backtracked all the way or stack was empty, reset the position
            ResetPosition();
        }

        public bool CanMakeMoreMoves(int moves)
        {
            return maxMoves > moves;
        }

    }
}
