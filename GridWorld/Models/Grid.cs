﻿using GridWorld.Enums;
using GridWorld.Interfaces;


namespace GridWorld.Models
{
    public class Grid : IGrid
    {
        public CellType[,] Tiles { get; private set; }


        private readonly Vector2Int start;

        private readonly Vector2Int goal;


        public Grid(int width, int height)
        {
            Tiles = new CellType[width, height];

            start = new Vector2Int(0, 0);
            goal = new Vector2Int(width - 1, height - 1);


            GenerateRandomWorld();
        }



        /// <summary>
        /// Generates a random world configuration for the grid.
        /// 
        /// This method works by iterating over each cell in the grid. For each cell, it uses a random value to determine the type of
        /// the cell. There's a 25% probability for a cell to be set as a Wall and an 75% probability for it to be set as Empty.
        /// 
        /// After generating the entire grid, the method ensures that the starting position (top-left corner) is set as Start and 
        /// the ending position (bottom-right corner) is set as Goal.
        /// </summary>
        private void GenerateRandomWorld()
        {
            var random = new Random();
            int cellTypeCount = Enum.GetNames(typeof(CellType)).Length;
            for (int x = 0; x < Tiles.GetLength(0); x++)
            {
                for (int y = 0; y < Tiles.GetLength(1); y++)
                {
                    Tiles[x, y] = random.Next(0, cellTypeCount) == 0 ? CellType.Wall : CellType.Empty;
                }
            }

            Tiles[start.X, start.Y] = CellType.Start;
            Tiles[goal.X, goal.Y] = CellType.Goal;

        }


        public Vector2Int GetGoalCell() => goal;

        public Vector2Int GetStartCell() => start;

        public bool IsValidCell(int x, int y)
        {
            return x >= 0 && x < Tiles.GetLength(0) && y >= 0 && y < Tiles.GetLength(1) && Tiles[x, y] != CellType.Wall;
        }

    }
}
