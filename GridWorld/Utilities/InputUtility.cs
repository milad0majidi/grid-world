﻿using GridWorld.Interfaces;

namespace GridWorld.Utilities
{
    public static class InputUtility
    {
        public static int GetInputValue(IGridView gridView, string message)
        {
            while (true)
            {
                gridView.DisplayMessage(message);
                string? input = Console.ReadLine();

                if (int.TryParse(input, out int value) && value > 0)
                {
                    return value;
                }
                else
                {
                    gridView.DisplayMessage("Invalid input. Please enter a valid integer.");
                }
            }
        }
    }
}
