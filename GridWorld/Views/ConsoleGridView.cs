﻿using GridWorld.Enums;
using GridWorld.Interfaces;

namespace GridWorld.Views
{
    public class ConsoleGridView : IGridView
    {
        public void DisplayGrid(CellType[,] grid, int playerX, int playerY)
        {
            Console.WriteLine();

            for (int y = 0; y < grid.GetLength(1); y++)
            {
                for (int x = 0; x < grid.GetLength(0); x++)
                {
                    if (x == playerX && y == playerY)
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    DisplayCell(grid[x, y]);
                    Console.ResetColor();
                }
                Console.WriteLine();
            }
        }


        private static void DisplayCell(CellType cellType)
        {
            switch (cellType)
            {
                case CellType.Empty:
                    Console.Write("O ");
                    break;
                case CellType.Wall:
                    Console.Write("# ");
                    break;
                case CellType.Start:
                    Console.Write("S ");
                    break;
                case CellType.Goal:
                    Console.Write("G ");
                    break;
            }
        }

        public void DisplayMessage(string message)
        {
            Console.WriteLine(message);
        }
    }
}
