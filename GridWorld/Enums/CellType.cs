﻿namespace GridWorld.Enums
{

    public enum CellType
    {
        Empty,
        Wall,
        Start,
        Goal
    }

}
