# GridWorld Solution README

## Overview
GridWorld is an engaging simulation game that challenges players to navigate through a dynamically generated grid to reach a designated goal. Within this grid, players encounter walls, open spaces, and special start and goal positions. Players move through the maze, capable of backtracking when faced with dead-ends and guided by a selected movement strategy.

## Key Components

### 1. `MainController`
The game's central controller, responsible for initializing essential game components — `IGrid`, `IPlayer`, and `IGridView` — and managing the primary game loop, ensuring a coherent gameplay progression.

### 2. Core Interfaces: `IGrid`, `IGridView`, `IPlayer`, `IMovementStrategy`
These interfaces are foundational to the game, outlining the grid's structure, its console-based visual presentation, player behavior and actions, and the overarching logic that influences player movement through the grid.

### 3. `CellType`
An enumeration that distinctly categorizes cell types within the grid. Cells are identified as `Empty`, `Wall`, `Start`, or `Goal`, each impacting player navigation in unique ways.

### 4. `Grid`
Representing `IGrid`, this class characterizes the game's field of play. It's tasked with constructing a random grid with a 75% chance for cells to be passable (`Empty`) and a 25% chance to be obstructive (`Wall`). Gameplay commences at the top-left (`Start`) and culminates at the bottom-right (`Goal`).

### 5. `Player`
This class, an instantiation of `IPlayer`, symbolizes the player character. It adopts a specific movement strategy, maintaining a history of both the path traversed and cells visited, critical for navigating efficiently and backtracking when necessary.

### 6. `Vector2DInt`
A vital class that represents a two-dimensional vector using integers. It's instrumental in defining positions and directions on the grid, facilitating precise movement and interactions within the grid environment.

### 7. `RandomMovementStrategy`
An embodiment of `IMovementStrategy`, this strategy introduces an element of unpredictability to the player's movements. Instead of a fixed, predictable route, the player's path through the grid unfolds randomly, contributing to the game's complexity and intrigue.

### 8. `InputUtility`
A utility component designed for robust management of the user's integer inputs, ensuring they fall within acceptable parameters, thereby guarding against input-related errors.

### 9. `ConsoleGridView`
This visual layer interprets the grid for console display, using distinct symbols for various elements:
   - `S` for the start position.
   - `G` for the goal.
   - `#` for walls.
   - `O` for open cells.
   - The player's current location is distinctively marked in red.

### 10. `Program`
The application's initiation point. It solicits the user for grid dimensions, initializes game elements based on these specifics, and activates the game loop.

## How to Play

1. Execute the application.
2. Define your preferred grid dimensions and stipulate a maximum number of moves when prompted.
3. With these parameters, the `MainController` propels the game into action.
4. Guided by the current movement strategy, the player ventures through the grid.
5. The game reaches its conclusion either when the goal is successfully met or when the player exhausts the allowed moves, with the outcome immediately presented.

## Gameplay Dynamics

1. The movement strategy employed significantly influences the player's next move, imbuing each decision with strategic importance.
2. Encountering a dead-end triggers backtracking to a previously visited cell.
3. Post-move, the console updates to reflect the current grid state and player's position, using the designated symbols.
4. Upon game end, the system reveals the total moves expended, accompanied by a message that declares whether the goal was achieved or the moves were fully utilized.
